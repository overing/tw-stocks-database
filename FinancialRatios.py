#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
FinancialRatios by PCHome Stock

Account: good_selection:pcj600i@pchome

首頁
http://pchome.syspower.com.tw/stock/sto2/ock2/sid2330.html

選了 [2013年第2季]
http://pchome.syspower.com.tw/stock/sto2/ock2/20132/sid2330.html
'''

'''
curl "http://pchome.syspower.com.tw/stock/sto2/ock2/20133/sid2330.html"
 -H "Accept-Encoding: gzip,deflate,sdch"
 -H "Accept-Language: zh-TW,zh;q=0.8,en-US;q=0.6,en;q=0.4"
 -H "User-Agent: Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36"
 -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
 -H "Referer: http://pchome.syspower.com.tw/stock/sto2/ock2/20132/sid2330.html"
 -H "Cookie: 
    stockid_records=YToxOntpOjA7czo0OiIyODA5Ijt9;
    stock_popup_personalnews=1;
    stock_id=god_selection;
    cookie=YToyOntzOjI6Iml2IjtzOjg6IplhqIhOKTdZIjtzOjU6ImNyeXB0IjtzOjQzOiKU5s_Jq2t_4sQCc2XohaZZXQCfPftHujXkzIut74uN_Emn8FtM1K9OUQAEIjt9;
    stock_config=YTozOntzOjc6IlN0b2NrSWQiO047czozOiJ0YWciO2k6MztzOjQ6InR5cGUiO2k6Mjt9"
 -H "Connection: keep-alive" --compressed
'''

import datetime
import pymongo
import random
import re
import socket
import sys
import time
import traceback
import urllib.request

socket.setdefaulttimeout(30)

DATABASE_HOST = 'localhost'
DATABASE_PORT = 27017
DATABASE_NAME = 'database_stock'
COLLECTION_NAME = 'FinancialRatios'

def try_decode(bs):
  for encode in ('utf8', 'big5', 'big5-hkscs', 'ansi'):
    try:
      return bs.decode(encode)
    except Exception as ex:
      pass
  return str(bs)

def get(code, year, season):
  url = 'http://pchome.syspower.com.tw/stock/sto2/ock2/%4d%1d/sid%04d.html'
  url = url % (year, season, code)
  print('url:', url)
  cookies = {
    'cookie': 'YToyOntzOjI6Iml2IjtzOjg6IpBs4VcZT382IjtzOjU6ImNyeXB0IjtzOjQzOiI9FPJ1osFFI6Bm56IReozI4BktBFtYdqbfqE89BIOvYFUcABoFykorDUH0Ijt9',
    'stock_config': 'YTozOntzOjc6IlN0b2NrSWQiO047czozOiJ0YWciO2k6MztzOjQ6InR5cGUiO2k6Mjt9',
    'stock_id': 'god_selection',
    'stock_popup_personalnews': '1',
    'stockid_records': 'YToxOntpOjA7czo0OiIyODA5Ijt9',
  }
  request = urllib.request.Request(url, headers={
    'Accept-Encoding': 'gzip,deflate,sdch', 
    'Accept-Language': 'zh-TW,zh;q=0.8,en-US;q=0.6,en;q=0.4',
    'User-Agent': 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Referer': 'http://pchome.syspower.com.tw/stock/sto2/ock2/20132/sid2330.html',
    'Cookie': '; '.join(['%s=%s' % (key, cookies[key]) for key in cookies.keys()])
  })
  with urllib.request.urlopen(request) as page:
    html = ''
    for line in page:
      html += try_decode(line)
  return html

def crop_table(html, keyword):
  '''
    html: page body
    keyword: unique keyword in side the table
  '''
  if type(html) == bytes: html = str(html)
  begin = html.rindex('<table', 0, html.index(keyword))
  end = html.index('/table>', begin) + len('/table>')
  return html[begin:end]

def parse_table(table):
  trs = re.findall(r'<tr[\d\D]*?>\s*([\d\D]*?)\s*</tr>', table)
  tr_title, tr_header, trs = trs[0], trs[1], trs[2:]
  
  seasons = []
  ths = re.findall(r'<th[\d\D]*?>\s*([\d\D]*?)\s*</th>', tr_header)
  ths = ths[1:]
  for th in ths:
    vs = re.findall('(\d+)年.*第(\d+)季', th)[0]
    seasons.append({
      'year': int(vs[0]),
      'season': int(vs[1])
    })
  # print seasons_range
  
  values = {}
  for tr in trs:
    tds = re.findall(r'<td[\d\D]*?>\s*([\d\D]*?)\s*</td>', tr)
    begin = len(tds) - len(seasons)
    key, tds = tds[0] if begin == 1 else tds[1], tds[begin:]
    seasons_values = []
    for index in range(0, len(tds)):
      td = tds[index]
      #print('td: %s' % td)
      if td.find('<span') > -1:
        td = re.findall(r'<span[\d\D]*?>\s*([\d\D]*?)\s*</span>', td)[0]
      value = 0.0 if td == '-' else float(td)
      seasons_values.append(value)
    values[key] = seasons_values
  #print('values: %s' % values)  
  print('seasons: %s' % seasons)
  table = []
  for idx_of_season in range(0, len(seasons)):
    row = dict(seasons[idx_of_season])
    for key in values.keys():
      #print('values["%s"]: %s' % (key, values[key]))
      row[key] = values[key][idx_of_season]
    table.append(row)
  return table

def capture():    
  start_time = datetime.datetime.now()
  print('Start', start_time)

  with pymongo.MongoClient(DATABASE_HOST, DATABASE_PORT) as client:
    collection = client[DATABASE_NAME][COLLECTION_NAME]

    import Stock
    target_codes = Stock.target_codes()
    
    # target_codes = list(reversed(target_codes))
    #print(target_codes)

    # 5/15, 8/15, 11/15
    today = start_time.date()
    year_end = today.year
    if today.month < 5 or (today.month == 5 and today.day < 15):
      year_end -= 1;

    for code in target_codes:
      if type(code) is not int: code = int(code)
      for year in range(2009, year_end + 1):
        season_end = 4
        if year == today.year:
          season_end = 3 if today.month >= 11 else (2 if tody.month >= 8 else 1)
        elif year == today.year - 1 and today.month < 4:
          season_end = 3 
        for season in range(1, season_end + 1):
          spec = { 'code': code, 'year': year, 'season': season }
          with collection.find(spec) as cursor:
            #for document in cursor: print('Document: %s' % document)
            #print('count: %d' % cursor.count())
            if cursor.count() > 0:
              print('Pass by storaged spec: %s' % spec)
              continue

          try:
            html = get(code, year, season)
          except Exception as ex:
            print('Get page fail c=%d y=%d s=%d' % (code, year, 3))
            traceback.print_exc()
            time.sleep(random.random() * 5 + 5)
            continue
          
          try:
            table = crop_table(html, '<span class="t15b">財務比率</span>')
            # except ValueError as ve:
          except Exception as ex:
            print('Crop table fail c=%d y=%d s=%d' % (code, year, 3))
            traceback.print_exc()
            time.sleep(random.random() * 5 + 5)
            continue

          try:
            table = parse_table(table)
          except Exception as ex:
            print('Parse table fail c=%d y=%d s=%d' % (code, year, 3))
            traceback.print_exc()
            continue

          for row in table:
            row['code'] = code
            spec = { 'code': code, 'year': row['year'], 'season': row['season'] }
            print('spec: %s' % spec)
            print('row: %s' % row)
            result = collection.update(spec, row, upsert=True)
            print('id: %s' % id)
            if not result['updatedExisting']:
              args = (code, spec['year'], spec['season'], str(result['upserted']))
              print('Upsert #%d %dQ%d: %s' % args)

            time.sleep(random.random() * 5 + 5)
    
  done_time = datetime.datetime.now()
  print('Done', done_time, 'dt:', str(done_time - start_time))

if __name__ == '__main__':
  # print(get(2330, 2013, 3))
  capture()
