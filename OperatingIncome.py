#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
    月營業收入 資料表相關操作
    
    資料來源
        公開資訊觀測站 查詢頁面
        [營運概況] > [每月營收] > [採用IFRSs後之月營業收入資訊]
        http://mops.twse.com.tw/mops/web/t05st10_ifrs
        http://mops.twse.com.tw/mops/web/ajax_t05st10_ifrs
        

    document sample
    {
        '備註'     : '',
        '營收'     : 46829051,
        'month'    : 2,
        'year'     : 103,
        'code'     : 2330,
        '_id'      : ObjectId('...')
    }
'''

import datetime
import pymongo
import re
import time

DATABASE_HOST = 'localhost'
DATABASE_PORT = 27017
DATABASE_NAME = 'database_stock'
COLLECTION_NAME = 'OperationIncome'

def capture(code_begin, code_end):
  
  start_time = datetime.datetime.now()
  
  interval = 30
  
  def request(code, year, month):
    q = {
      'run': 'Y',
      'step': 0,
      'co_id': code,
      'year': year,
      'month': '%02d' % month,
      'firstin': 'true'
    }
    url = 'http://mops.twse.com.tw/mops/web/ajax_t05st10_ifrs'
    url = 'http://210.69.240.131/mops/web/ajax_t05st10_ifrs'
    url += '?' + '&'.join('%s=%s' % (key, q[key]) for key in q.keys())
    print('url:', url)
    return urllib2.urlopen(url).read()
  
  import Stock
  target_codes = Stock.target_codes()
  
  # target_list = list(reversed(target_list))
  # print(target_list)
  
  client = pymongo.MongoClient(DATABASE_HOST, DATABASE_PORT)
  collection = client[DATABASE_NAME][COLLECTION_NAME]
  colle_fail = client['database_fail'][COLLECTION_NAME]
  
  for code in target_codes:
    end_year = start_time.year - 1911
    for year in range(102, end_year + 1):
      end_month = 12 if year < end_year else start_time.month
      for month in range(1, end_month):
        
        def check_limit_case(html, kwyword):
          kwyword = kwyword.decode('utf-8').encode('big5')
          if html.find(big5(keyword)) >= 0:
            print keyword, '(wait %ds):' % interval,
            time.sleep(interval)
            html = request(code, year, month)
            if html.find(big5(keyword)) >= 0:
              print keyword, 'Fail'
              time.sleep(interval)
              html = None
          return html
  
        def check_no_data(html, keyword):
          if html.find(keyword) >= 0:
            print keyword, ':No data'
            colle_fail.update(spec, document_fail, upsert=True)
            time.sleep(interval)
            html = None
          return html
        
        spec = { 'code': code, 'year': year, 'month': month }
        
        document_fail = colle_fail.find_one(spec)
        if document_fail and document_fail['no_data']:
          print 'Pass sure no data #%s %s/%s:' % (code, year, month)
          continue
        
        document_fail = dict(spec)
        document_fail['no_data'] = True
        
        print 'Request #%s %s/%s:' % (code, year, month),
        html = request(code, year, month)
        
        if not check_limit_case(html, '查詢過於頻繁，請於稍後再查詢!'):
          continue
        if not check_limit_case(html, '系統流量大，請稍後再查詢!'):
          continue
        if not check_no_data(html, '公開發行公司不繼續公開發行！'):
          continue
        if not check_no_data(html, '上市公司已下市！'):
          continue
        if not check_no_data(html, '上櫃公司已下櫃！'):
          continue
        if not check_no_data(html, '外國發行人免申報本項資訊'):
          continue
        if html.find('查無此公司資料') >= 0: # 可能未來會有
          print keyword, ':No data'
          time.sleep(interval)
          continue
        
        try:
          keyword = '<th class="tblHead">營業收入淨額</th>'
          if type(html) == bytes: html = str(html)
          begin = html.rindex('<table', 0, html.index(keyword))
          end = html.index('/table>', begin) + len('/table>')
          table = html[begin:end]
        except Exception as ex:
          print 'Fail'
          time.sleep(interval)
          continue

        html = html.lower()
        html = html.replace('&nbsp;', '')
        html = html.replace('<th', '<td').replace('/th>', '/td>')
        
        trs = re.findall(r'<tr[\d\D]*?>\s*([\d\D]*?)\s*</tr>', html)
        if html.find('功能性貨幣') > -1:
          trs = trs[1:]
                
        pattern = r'<td[\d\D]*?>\s*([\d\D]*?)\s*</td>'
        document = dict(spec)
        
        td = re.findall(pattern, trs[0])[1]
        document['營收'] = int(td.replace(',', ''))
        
        td = re.findall(pattern, trs[8])[1]
        document['備註'] = int(td.replace(',', ''))
        
        collection.update(spec, document, upsert=True)
        print 'Success'
                
  client.close()

def operation_income(code, year, month):
  ''' 用證券代號, 年份, 月份, 查詢營收資料 '''
  
  if not isinstance(code, int): code = int(code)
  if not isinstance(year, int): year = int(year)
  if not isinstance(month, int): code = int(month)
  with pymongo.MongoClient(DATABASE_HOST, DATABASE_PORT) as client:
    collection = client[DATABASE_NAME][COLLECTION_NAME]
    spec = { 'code': code, 'year': year, 'month': month }
    document = collection.find_one(spec)
  return None if not document else dict(document)

if __name__ == '__main__':
  print operation_income(2330, 103, 2)
