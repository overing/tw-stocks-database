#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
    證券資料表相關操作
    
    資料來源
        台灣證券交易所 查詢頁面
        http://isin.twse.com.tw/isin/class_main.jsp

    document sample
    {
        'CFI'      : 'ESVUFR',
        '產業'     : '半導體業',
        '國際編碼' : 'TW0002330008',
        '日期'     : '1994/09/05',
        '市場'     : '上市',
        '備註'     : '',
        '類型'     : '股票',
        '名稱'     : '台積電',
        '代碼'     : '2330',
        '_id'      : ObjectId('...')
    }
'''

import datetime
import pymongo
import re
import sys
import urllib.request

DATABASE_HOST = 'localhost'
DATABASE_PORT = 27017
DATABASE_NAME = 'database_stock'
COLLECTION_NAME = 'Stock'

def capture():
  ''' 從證交所抓取資料回來 '''
  
  start_time = datetime.datetime.now()
  print('Start %s' % start_time)
  
  # url = 'http://isin.twse.com.tw/isin/class_main.jsp'
  url = 'http://210.71.239.231/isin/class_main.jsp'
  with urllib.request.urlopen(url) as page:
    html = ''
    for line in page:
      for encode in ('utf8', 'big5', 'big5-hkscs', 'ansi'):
        try:
          line = line.decode(encode)
        except Exception as ex:
          line = str(line)
      html += line
    
    trs = re.findall('<tr[\d\D]*?>\s*([\d\D]*?)\s*</tr>', html)
    trs = trs[1:]
    
    with pymongo.MongoClient(DATABASE_HOST, DATABASE_PORT) as client:
      collection = client[DATABASE_NAME][COLLECTION_NAME]
      collection.ensure_index('代碼', unique=True)
      
      for i in range(0, len(trs)):
        tds = re.findall('<td[\d\D]*?>\s*([\d\D]*?)\s*</td>', trs[i])
        # tr sample: 1, TW0002330008, 2330, 台積電, 上市, 股票, 半導體業, 1994/09/05, ESVUFR,
        stock = {
          '國際編碼': tds[1],
          '代碼': tds[2],
          '名稱': tds[3],
          '市場': tds[4],
          '類型': tds[5], 
          '產業': tds[6], 
          '日期': tds[7], 
          'CFI': tds[8], 
          '備註': tds[9]
        }
        collection.update({ '代碼': stock['代碼'] }, stock, upsert=True)
        print('%s %s (%d/%d)' % (tds[2], tds[3], i + 1, len(trs)))
    
  end_time = datetime.datetime.now()
  print('Done %s dt: %s' % (end_time, end_time - start_time))

def stock_by_code(code):
  ''' 用證券代號查詢證券資料 '''
  
  if not isinstance(code, str) and not isinstance(code, unicode):
    code = str(code)
  with pymongo.MongoClient(DATABASE_HOST, DATABASE_PORT) as client:
    collection = client[DATABASE_NAME][COLLECTION_NAME]
    document = collection.find_one({ '代碼': code })
  return None if not document else dict(document)

def target_codes():
  ''' 取得操作目標的股票代號 list '''
  
  spec = { '$and': [
    { '$or': [ { '市場': '上市' }, { '市場': '上櫃' } ] },
    { '類型': '股票' }
  ] }
  fields = [ '代碼' ]
  sort = [ ('代碼', pymongo.ASCENDING) ]
  with pymongo.MongoClient(DATABASE_HOST, DATABASE_PORT) as client:
    collection = client[DATABASE_NAME][COLLECTION_NAME]
    with collection.find(spec, fields=fields, sort=sort) as cursor:
      rval = [stock['代碼'] for stock in cursor]
  return rval

if __name__ == '__main__':
  # with pymongo.MongoClient(DATABASE_HOST, DATABASE_PORT) as client:
    # collection = client[DATABASE_NAME][COLLECTION_NAME]
    # collection.drop()
  
  # capture()
  
  # print(target_codes())
  
  print(stock_by_code('2330'))
