#!/usr/bin/python3
# -*- coding: utf-8 -*-


''' 股價歷史 '''

import os
import pymongo

DATABASE_HOST = 'localhost'
DATABASE_PORT = 27017
DATABASE_NAME = 'database_stock'
COLLECTION_NAME = 'StockPlace'

def import_peters_csv(folder_path):
  ''' 從指令資料夾輸入 peters 前輩的 csv 檔倒入 DB '''
  import re
  re_filenmae = re.compile('^\d{4}\.csv$')
  re_code = re.compile('^(\d{4})\.csv$')
  re_date_split = re.compile('^(\d{4})(\d{2})(\d{2})$')

  with pymongo.MongoClient(DATABASE_HOST, DATABASE_PORT) as client:
    collection = client[DATABASE_NAME][COLLECTION_NAME]

    for file in os.listdir(folder_path):
      matchs_filenmae = re_filenmae.findall(file)
      matchs_code = re_code.findall(file)
      if len(matchs_filenmae):
        file = matchs_filenmae[0]
        code = int(matchs_code[0])
        file_path = os.path.join(folder_path, file)
        print('import_file', code, file_path)
        with open(file_path, 'r') as fd:
          header = fd.readline().split(',')[0:6]
          # print 'header', header
          for line in fd:
            cells = line.replace('\n', '').split(',')
            date = re_date_split.findall(cells[0])[0]
            spec = {
              'code': code,
              'year': int(date[0]),
              'month': int(date[1]),
              'day': int(date[2])
            }
            document = dict(spec)
            document['open'] = float(cells[1])
            document['hight'] = float(cells[2])
            document['low'] = float(cells[3])
            document['close'] = float(cells[4])
            document['vol'] = int(cells[5])
            collection.update(spec, document, upsert=True)

def stock_history(code):
  with pymongo.MongoClient(DATABASE_HOST, DATABASE_PORT) as client:
    collection = client[DATABASE_NAME][COLLECTION_NAME]
    with collection.find({ 'code': int(code) }) as cursor:
      rval = [daily for daily in cursor]
      return rval

def count():
  with pymongo.MongoClient(DATABASE_HOST, DATABASE_PORT) as client:
    collection = client[DATABASE_NAME][COLLECTION_NAME]
    return collection.count()

if __name__ == '__main__':
  # import_peters_csv(r'D:\CO\Auto\tools\stock_db')
  
  # print '%s.%s %d rows' % (DATABASE_NAME, COLLECTION_NAME, count())
  
  for daily in stock_history(2330):
    print('#%d %d/%02d/%02d O: %f, H: %f, L: %f, C: %f, Q: %d' % (
      daily['code'], daily['year'], daily['month'], daily['day'], 
      daily['open'], daily['hight'], daily['low'], daily['close'],
      daily['vol']
    ))
    