#!/usr/bin/python
# -*- coding: utf-8 -*-

import bson
import json
import pymongo
import re
import time
import traceback
import urlparse

import BaseHTTPServer

from bson import json_util

DB_HOST = 'localhost'
DB_PORT = 27017

API_PORT = 17888

class Handler(BaseHTTPServer.BaseHTTPRequestHandler):
    def setup(self):
        BaseHTTPServer.BaseHTTPRequestHandler.setup(self)
        self.db_client = pymongo.MongoClient(DB_HOST, DB_PORT)
    
    def finish(self):
        self.db_client.close()
        BaseHTTPServer.BaseHTTPRequestHandler.finish(self)
    
    def do_GET(self):
        path_info = urlparse.urlparse(self.path)
        path = path_info.path
        query = urlparse.parse_qs(path_info.query)
        print 'path: %s\nquery: %s' % (path, query)
        
        if path == '/':
            page = ''
            with open('main.html', 'r') as f:
                page = f.read()
            self.responce(page, 'text/html')

        elif path.startswith('/collection'):
            for key in query.keys():
                query[key] = query[key][0]
            responce = self.query_collection(query, path.split('/')[2])
            print responce
            content = str(json.dumps(responce))
            self.responce(content, 'application/json')

        elif path.startswith('/function'):
            for key in query.keys():
                query[key] = query[key][0]
            responce = self.query_function(path.split('/')[2], query)
            content = str(json.dumps(responce))
            self.responce(content, 'application/json')

        else:
            self.responce('404 - Not found.', 'text/plain', 404)
    
    def query_collection(self, spec, collection, db='db_stock'):
        print "query_collection ['%s']['%s'].find(%s)" % (db, collection, spec)
        try:
            cursor = self.db_client[db][collection].find(spec)
            if collection == 'colle_stock' and cursor.count() > 0:
                cursor.sort( [
                    ('own_code', pymongo.ASCENDING), 
                    ('年', pymongo.ASCENDING), 
                    ('月', pymongo.ASCENDING)
                ] )
            data = []
            for stock in cursor:
                record = {}
                for key in stock.keys():
                    record[key] = unicode(stock[key]) if key == '_id' else stock[key]
                data.append(record)
            cursor.close()
            return { 'success': True, 'data': data }

        except Exception as ex:
            traceback.print_exc()
            return { 'success': False, 'error': str(ex) }
    
    def query_function(self, function, params):
        print "query_function %s(%s)" % (function, params)
        try:
            if function == 'list_database':
                return { 'success': True, 'data': self.db_client.database_names() }
            elif function == 'list_collection':
                database = params['database']
                return { 'success': True, 'data': self.db_client[database].collection_names(False) }
        except Exception as ex:
            return { 'success': False, 'error': str(ex) }
        
        # TODO
        return { 'success': False, 'data': '施工中: %s' % params }
    
    def responce(self, content='OK', mime='text/plain', sc=200):
        self.send_response(sc)
        self.send_header('Content-type', mime)
        self.end_headers()
        self.wfile.write(content)
    

if __name__ == '__main__':
    httpd = BaseHTTPServer.HTTPServer(('', API_PORT), Handler)
    print time.asctime(), 'Server Starts - localhost: %d' % API_PORT
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print time.asctime(), 'Server Stops - localhost: %d' % API_PORT

