#!/usr/bin/python
# -*- coding: utf-8 -*-

''' 擷取 營運概況>每月營收
原始介面: http://mops.twse.com.tw/mops/web/t05st10_ifrs
資料介面: http://mops.twse.com.tw/mops/web/ajax_t05st10_ifrs
'''

import datetime
import re
import pymongo
import socket
import sys
import time
import traceback
import urllib2

import common

socket.setdefaulttimeout(30)

SOURCE_HOST = 'mops.twse.com.tw'
SOURCE_HOST = '210.69.240.131'
SOURCE_API_URL = 'http://%s/mops/web/ajax_t05st10_ifrs'
SOURCE_API_URL = SOURCE_API_URL % SOURCE_HOST

debug = False

def get_and_parse(code, year, month):
    params = {
        'step': 0,
        'firstin': 1,
        'co_id': '%04d' % code,
        'year': year,
        'month': '%02d' % month
    }
    params = '%s=%s' % (key, params[key]) for key in params.keys()
    url = SOURCE_API_URL + '?' + '&'.join(params)

    html = urllib2.urlopen(url).read()
    
    # 可能以後會有資料
    if html.find('查無符合條件資料！') > -1:
        raise Exception('No result', html)
    
    # 查被限制
    if html.find(u'請於稍後再查詢!'.encode('big5')) > -1:
        raise Exception('Limit', html)
    
    # 確定以後會沒資料
    if html.find(u'公開發行公司不繼續公開發行！'.encode('big5')) > -1:
        raise Exception('Sure no', html)
    if html.find(u'上市公司已下市！'.encode('big5')) > -1:
        raise Exception('Sure no', html)
    if html.find(u'上櫃公司已下櫃！'.encode('big5')) > -1:
        raise Exception('Sure no', html)
    if html.find(u'外國發行人免申報本項資訊'.encode('big5')) > -1:
        raise Exception('Sure no', html)
    if html.find(u'查無此公司資料'.encode('big5')) > -1:
        raise Exception('Sure no', html)
    """ TODO check fail responce """

    html = html.lower()
    table = common.crop_data_table(html, '營業收入淨額', '增減百分比')
    if not table:
        raise Exception('No table element', html)
    trs = re.findall(r'<tr[\d\D]*?>\s*([\d\D]*?)\s*</tr>', table)
    
    document = { 'code': code, '年': year, '月': month }
    
    text = common.find_tds_text_by_first('本月', trs)[1][0]
    document['本月'] = int(text.replace(',', ''))
    
    text = common.find_tds_text_by_first('去年同期', trs)[1][0]
    document['去年同期'] = int(text.replace(',', ''))
    
    text = common.find_tds_text_by_first('增減金額', trs)[1][0]
    document['較去年同期增減金額'] = int(text.replace(',', ''))
    
    text = common.find_tds_text_by_first('增減百分比', trs)[1][0]
    document['較去年同期增減百分比'] = float(text)
    
    text = common.find_tds_text_by_first('本年累計', trs)[1][0]
    document['本年累計'] = int(text.replace(',', ''))
    
    text = common.find_tds_text_by_first('去年累計', trs)[1][0]
    document['去年累計'] = int(text.replace(',', ''))
    
    text = common.find_tds_text_by_first('增減金額', trs)[1][0]
    document['較去年累計增減金額'] = int(text.replace(',', ''))
    
    text = common.find_tds_text_by_first('增減百分比', trs)[1][0]
    document['較去年累計增減百分比'] = float(text)
    
    c = common.find_tds_text_by_first('備註', trs)[1]
    text = '' if len(c) == 0 else c[0]
    document['備註'] = text
    
    return document

if __name__ == '__main__':
    append = False
    code_begin = 0
    code_end = 9999
    for arg in sys.argv[1:]:
        if arg == '--append': append = True
        if arg == '--debug': debug = True
        matchs = re.findall('--code_begin=(\d{4})', arg)
        if len(matchs) == 1: code_begin = int(matchs[0])
        matchs = re.findall('--code_end=(\d{4})', arg)
        if len(matchs) == 1: code_end = int(matchs[0])
    if debug:
        print 'append:', append, ', debug:', debug,
        print ',code_begin:', code_begin, ',code_end:', code_end

    start_time = datetime.datetime.now()
    print 'start time:', start_time
    
    try:
        client = pymongo.MongoClient('localhost', 27017)

        spec = { '$and': [
            { 'own_code': { '$regex': '^\d{4}$' } },
            { 'market': { '$ne': '興櫃' } },
            { '$or': [
                { 'issuer_type': '股票' },
                { 'issuer_type': '普通股' }
            ]}
        ]}
        fields = [ 'own_code' ]
        sort = [ ( 'own_code', pymongo.ASCENDING ) ]
        collection = client['db_stock']['colle_stock']
        cursor = collection.find(spec, fields, sort=sort)

        targets_code = []
        for row in cursor:
            code = int(row['own_code'])
            if code >= code_begin and code <= code_end:
                targets_code.append(code)
        cursor.close()
        if debug: print 'targets_code:', targets_code

        collection = client['database_stock']['t05st10_ifrs']
        for code in targets_code:
            year_end = start_time.year - 1911
            for year in xrange(102, year_end + 1):
                month_end = start_time.month - 1
                for month in xrange(1, month_end + 1):
                    document = get_and_parse(code, year, month)
        
        # document = get_and_parse(2330, 102, 6)
        # for key in document.keys():
            # print '%s=%s' % (key, document[key])
    finally:
        client.close()

