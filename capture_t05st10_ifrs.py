#!/usr/bin/python
# -*- coding: utf-8 -*-

''' 每月營收 '''

import datetime
import re
import pymongo
import random
import sys
import time
import traceback
import urllib2

API_URL = 'http://mops.twse.com.tw/mops/web/ajax_t05st10_ifrs'
API_URL = 'http://210.69.240.131/mops/web/ajax_t05st10_ifrs'

debug = False

def query_data(own_code, year, month, colle_fail):
    params = {
        'run': 'Y',
        'step': 0,
        'co_id': own_code,
        'year': year,
        'month': '%02d' % month,
        'firstin': 'true'
    }
    url = API_URL
    url += '?' + '&'.join('%s=%s' % (key, params[key]) for key in params.keys())
    if debug: print 'url:', url

    spec = { 'own_code': own_code, '年': year, '月': month }
    doc_fail = dict(spec)
    doc_fail['no_data'] = 1
    doc_fail['fail'] = 0

    #print 'Request: ', url
    content = urllib2.urlopen(url).read()
    if content.find('查詢過於頻繁，請於稍後再查詢!'.decode('utf-8').encode('big5')) > -1:
        print '詢過於頻繁，請於稍後再查詢!(wait 30s):',
        time.sleep(30)
        content = urllib2.urlopen(url).read()
        if content.find('查詢過於頻繁，請於稍後再查詢!'.decode('utf-8').encode('big5')) > -1:
            print '查詢過於頻繁，請於稍後再查詢!(pass):',
            return None
    if content.find('系統流量大，請稍後再查詢!'.decode('utf-8').encode('big5')) > -1:
        print '系統流量大，請稍後再查詢!(wait 30s):',
        time.sleep(30)
        content = urllib2.urlopen(url).read()
        if content.find('系統流量大，請稍後再查詢!'.decode('utf-8').encode('big5')) > -1:
            print '系統流量大，請稍後再查詢!(pass)',
            return None
    if content.find('公開發行公司不繼續公開發行！') > -1:
        print '公開發行公司不繼續公開發行！:',
        colle_fail.update(spec, doc_fail, upsert=True)
        return None
    if content.find('上市公司已下市！') > -1:
        print '上市公司已下市！:',
        colle_fail.update(spec, doc_fail, upsert=True)
        return None
    if content.find('上櫃公司已下櫃！') > -1:
        print '上櫃公司已下櫃！:',
        colle_fail.update(spec, doc_fail, upsert=True)
        return None
    if content.find('外國發行人免申報本項資訊') > -1:
        print '外國發行人免申報本項資訊:',
        colle_fail.update(spec, doc_fail, upsert=True)
        return None
    if content.find('查無此公司資料') > -1:
        print '查無此公司資料',
        colle_fail.update(spec, doc_fail, upsert=True)
        return None
    if content.find('資料庫中查無需求資料') > -1:
        print '資料庫中查無需求資料',
        # colle_fail.update(spec, doc_fail, upsert=True)
        return None
    begin = content.find('營業收入淨額')
    if begin < 0:
        try:
            content = content.decode('big5')
        except:
            pass
        print content
        return None
    end = content.find('</table>', begin)
    content = content[begin:end]
    content = content.replace('&nbsp;', '')
    content = content.replace('<th', '<TD')
    content = content.replace('<TH', '<TD')
    content = content.replace('/th>', '/TD>')
    content = content.replace('/TH>', '/TD>')

    ''' sample
        本月, 54,027,749
        去年同期, 43,466,925
        增減金額, 10,560,824
        增減百分比, 24.30
        本年累計, 288,641,316
        去年累計, 233,801,162
        增減金額, 54,840,154
        增減百分比, 23.46
    '''
    trs = re.findall(r'<TR[\d\D]*?>\s*([\d\D]*?)\s*</TR>', content, re.IGNORECASE)
    if content.find('功能性貨幣') > -1:
        trs = trs[1:]
    data = {}

    td = re.findall(r'<TD[\d\D]*?>\s*([\d\D]*?)\s*</TD>', trs[0], re.IGNORECASE)[1]
    data['本月'] = int(td.replace(',', ''))

    td = re.findall(r'<TD[\d\D]*?>\s*([\d\D]*?)\s*</TD>', trs[1], re.IGNORECASE)[1]
    data['去年同期'] = int(td.replace(',', ''))

    td = re.findall(r'<TD[\d\D]*?>\s*([\d\D]*?)\s*</TD>', trs[2], re.IGNORECASE)[1]
    data['較去年同期增減金額'] = int(td.replace(',', ''))

    td = re.findall(r'<TD[\d\D]*?>\s*([\d\D]*?)\s*</TD>', trs[3], re.IGNORECASE)[1]
    data['較去年同期增減百分比'] = float(td.replace(',',''))

    td = re.findall(r'<TD[\d\D]*?>\s*([\d\D]*?)\s*</TD>', trs[4], re.IGNORECASE)[1]
    data['本年累計'] = int(td.replace(',', ''))

    td = re.findall(r'<TD[\d\D]*?>\s*([\d\D]*?)\s*</TD>', trs[5], re.IGNORECASE)[1]
    data['去年累計'] = int(td.replace(',', ''))

    td = re.findall(r'<TD[\d\D]*?>\s*([\d\D]*?)\s*</TD>', trs[6], re.IGNORECASE)[1]
    data['較去年累計增減金額'] = int(td.replace(',', ''))

    td = re.findall(r'<TD[\d\D]*?>\s*([\d\D]*?)\s*</TD>', trs[7], re.IGNORECASE)[1]
    data['較去年累計增減百分比'] = float(td.replace(',', ''))

    return data

if __name__ == '__main__':
    append = False
    code_begin = 0
    code_end = 9999
    for arg in sys.argv[1:]:
        if arg == '--append': append = True
        if arg == '--debug': debug = True
        matchs = re.findall('--code_begin=(\d{4})', arg)
        if len(matchs) == 1: code_begin = int(matchs[0])
        matchs = re.findall('--code_end=(\d{4})', arg)
        if len(matchs) == 1: code_end = int(matchs[0])
    if debug:
        print 'append:', append, ', debug:', debug,
        print ',code_begin:', code_begin, ',code_end:', code_end
    
    start = datetime.datetime.now()
    print 'start', start

    s = f = p = n = 0

    client =  pymongo.MongoClient('localhost', 27017)
    
    spec = { '$and': [
        { 'own_code': { '$regex': '^\d{4}$' } },
        { 'market': { '$ne': '興櫃' } },
        { '$or': [
            { 'issuer_type': '股票' }, { 'issuer_type': '普通股' }
        ]}
    ]}
    cursor = client['db_stock']['colle_stock'].find(spec, timeout=False)
    
    targets_code = []
    for row in cursor:
        code = int(row['own_code'])
        if code >= code_begin and code <= code_end:
            targets_code.append(code)
    cursor.close()
    if debug: print 'targets_code:', targets_code
    
    colle_fail = client['db_stock']['colle_fail']
    # colle_fail.drop()
    # exit()
    
    colle_history = client['db_stock']['colle_history']
    for own_code in targets_code:
        max_year = start.year - 1911
        for year in xrange(102, max_year + 1):
            max_month = (start.month - 1) if year == max_year else 12
            for month in xrange(1, max_month + 1):
                print 'query_data(%s, %s, %02d):' % (own_code, year, month),
                spec = { 'own_code': str(own_code), '年': year, '月': month }
                if append and colle_history.find_one(spec):
                    p += 1
                    print 'Pass by storaged'
                    continue
                doc_fail = colle_fail.find_one(spec)
                if doc_fail:
                    if doc_fail['no_data'] == 1:
                        n += 1
                        print 'Pass by just no data'
                        continue
                    #if doc_fail['fail'] > 10:
                    #    f += 1
                    #    print 'Pass by fail > 10 time'
                    #    continue
                try:
                    data = query_data(own_code, year, month, colle_fail)
                    if data:
                        s += 1
                        print 'Success'
                        data['own_code'] = str(own_code)
                        data['年'] = year
                        data['月'] = month
                        colle_history.update(spec, data, upsert=True)
                    else:
                        n += 1
                        print 'No data'
                    time.sleep(1)
                except Exception as ex:
                    if not doc_fail:
                        doc_fail = dict(spec)
                        doc_fail['fail'] = 0
                        doc_fail['no_data'] = 0
                        colle_fail.update(spec, doc_fail, upsert=True)
                    f += 1
                    print 'Fail, ', str(ex)
                    traceback.print_exc(file=sys.stderr)

    client.close()

    done = datetime.datetime.now()
    print 'done', done, 'dt:', str(done - start)

    print 'Success: %d, Fail: %d, Pass: %d, NoData: %d' % (s, f, p, n)

