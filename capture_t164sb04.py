#!/usr/bin/python
# -*- coding: utf-8 -*-

''' 擷取 財務報表>綜合損益表
原始介面: http://mops.twse.com.tw/mops/web/t164sb04
資料介面: http://mops.twse.com.tw/mops/web/ajax_t164sb04
'''

import datetime
import re
import pymongo
import socket
import sys
import time
import traceback
import urllib2

import common

socket.setdefaulttimeout(30)

SOURCE_HOST = 'mops.twse.com.tw'
SOURCE_HOST = '210.69.240.131' #節省查詢時間
SOURCE_API_URL = 'http://%s/mops/web/ajax_t164sb04' % SOURCE_HOST

debug = False

def get_and_parse(code, year, season):
    params = {
        'step': 1,
        'firstin': 1,
        'co_id': '%04d' % code,
        'year': year,
        'season': '%02d' % season
    }
    url = SOURCE_API_URL
    url += '?' + '&'.join('%s=%s' % (key, params[key]) for key in params.keys())

    html = urllib2.urlopen(url).read()
    if html.find('查無符合條件資料！') > -1:
        raise Exception('No result', html)
    if html.find('請於稍後再查詢!'.encode('big5')) > -1:
        raise Exception('Limit', html)
    """ TODO check fail responce """

    html = html.lower()
    #table = common.crop_data_table(html, '營業收入合計', '稀釋每股盈餘')
    table = common.crop_data_table(html, '營業收入合計', '基本每股盈餘')
    if not table:
        raise Exception('No table element', html)
    trs = re.findall(r'<tr[\d\D]*?>\s*([\d\D]*?)\s*</tr>', table)
    
    """ 把要切出來的值倒成 map
        values 會長這樣
        {
            'key_word1': ['key_word1', 當季金額, 當季%, 前年同季金額, 前年同季%],
            'key_word2': ['key_word2', 當季金額, 當季%, 前年同季金額, 前年同季%],
            ...
        }
    """
    values = dict([ common.find_tds_text_by_first(kw, trs) for kw in [
        '營業收入合計',
        '營業成本合計',
        '營業毛利（毛損）',
        '已實現銷貨（損）益',
        '營業毛利（毛損）淨額',
        '推銷費用',
        '管理費用',
        '研究發展費用',
        '營業費用合計',
        '其他收益及費損淨額',
        '營業利益（損失）',
        '其他收入',
        '其他利益及損失淨額',
        '財務成本淨額',
        '採用權益法認列之關聯企業及合資損益之份額淨額',
        '營業外收入及支出合計',
        '稅前淨利（淨損）',
        '所得稅費用（利益）合計',
        '繼續營業單位本期淨利（淨損）',
        '本期淨利（淨損）',
        '國外營運機構財務報表換算之兌換差額',
        '備供出售金融資產未實現評價損益',
        '現金流量避險',
        '採用權益法認列之關聯企業及合資之其他綜合損益之份額合計',
        '與其他綜合損益組成部分相關之所得稅',
        '其他綜合損益（淨額）',
        '本期綜合損益總額',
        '母公司業主（淨利／損）',
        '非控制權益（淨利／損）',
        '母公司業主（綜合損益）',
        '非控制權益（綜合損益）'
    ] ])
    if debug:
        print 'values:'
        for k in values:
            print '%s=%s' % (k, values[k][1:])

    # 處理成要塞進 collection 的長相
    document = { 'code': code, '年': year, '季': season }
    for key in values.keys():
      #try:
        vok = values[key]
        """ 如果後面四個 td 內容都是空的就跳過 """
        if ''.join(vok[1:]).strip() == '': continue
        document[key] = common.parse_number(vok[1])
        document[key + '%'] = common.parse_number(vok[2])
      #except:
      #  print 'Fail on parse key: %s' % key

    """ 這兩條 tr 長像不一樣要特別處裡 """
    key, vok = common.find_tds_text_by_first('基本每股盈餘', trs) # 跳過無值 row
    key, vok = common.find_tds_text_by_first('基本每股盈餘', trs)
    document['基本每股盈餘'] = common.parse_number(vok[1])
    
    #key, vok = common.find_tds_text_by_first('稀釋每股盈餘', trs) # 跳過無值 row
    #key, vok = common.find_tds_text_by_first('稀釋每股盈餘', trs)
    #document['稀釋每股盈餘'] = common.parse_number(vok[1])

    return document

if __name__ == '__main__':
    append = False
    code_begin = 0
    code_end = 9999
    for arg in sys.argv[1:]:
        if arg == '--append': append = True
        if arg == '--debug': debug = True
        matchs = re.findall('--code_begin=(\d{4})', arg)
        if len(matchs) == 1: code_begin = int(matchs[0])
        matchs = re.findall('--code_end=(\d{4})', arg)
        if len(matchs) == 1: code_end = int(matchs[0])
    if debug:
        print 'append:', append, ', debug:', debug,
        print ',code_begin:', code_begin, ',code_end:', code_end

    #print get_and_parse(1215, 102, 1)
    #exit()

    start_time = datetime.datetime.now()
    print 'start time:', start_time

    try:
        client = pymongo.MongoClient('localhost', 27017)

        # like SQL where
        spec = { '$and': [
            { 'own_code': { '$regex': '^\d{4}$' } },
            { 'market': { '$ne': '興櫃' } },
            { '$or': [
                { 'issuer_type': '股票' }, { 'issuer_type': '普通股' }
            ]}
        ]}
        fields = [ 'own_code' ] # like SQL select
        sort = [ ( 'own_code', pymongo.ASCENDING ) ] # like SQL order by
        cursor = client['db_stock']['colle_stock'].find(spec, fields, sort=sort)

        targets_code = []
        for row in cursor:
            code = int(row['own_code'])
            if code >= code_begin and code <= code_end:
                targets_code.append(code)
        cursor.close()
        if debug: print 'targets_code:', targets_code

        collection = client['database_stock']['t164sb04']
        for code in targets_code:
            year_end = start_time.year - 1911
            for year in xrange(102, year_end + 1):
                month_end = start_time.month - 1
                season_end = 4 if year < year_end else month_end / 4
                for season in xrange(1, season_end + 1):
                    done = False
                    while not done:
                        spec = { 'code': code, '年': year, '季': season }
                        if append and collection.find_one(spec):
                            done = True
                            print 'append mode pass:', spec
                            continue
                        
                        print 'get_and_parse(%04d, %d, %02d):' % (code, year, season),
                        try:
                            document = get_and_parse(code, year, season)
                            if not debug: print 'Ok'
                            else: print 'document:', document
                            collection.update(spec, document, upsert=True)
                            done = True
                        except Exception as ex:
                            if ex[0] == 'No result':
                                print ex[0]
                                done = True
                            elif ex[0] == 'Limit':
                                print 'Query limit, wait 30s'
                                time.sleep(30)
                            else:
                                traceback.print_exc(file=sys.stdout)
                                done = True
                        time.sleep(1.6)
                            #traceback.print_exc(file=sys.stdout)
                        #sys.stderr.write(str(ex) + '\n')
                        
        end_time = datetime.datetime.now()
        print (end_time - start_time)
    finally:
        client.close()

