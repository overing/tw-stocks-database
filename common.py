#!/usr/bin/python
# -*- coding: utf-8 -*-

import re

def try_decode(bs):
  for encode in ('utf8', 'big5', 'big5-hkscs'):
    try:
      return bs.decode(encode)
    except Exception as ex:
      pass
  return str(bs)

def crop_data_table(html, keyword):
  begin_pos = html.rfind('<table', 0, html.find(keyword))
  end_pos = html.find('/table>', begin_pos) + len('/table>')
  return html[begin_pos:end_pos]

def find_tds_text_by_first(text_of_first_td, trs):
    for tr in trs:
        tr_clear = tr.replace('&nbsp;', '')
        if tr.find(text_of_first_td) > -1:
            pattern = '<td[\d\D]*?>\s*([\d\D]*?)\s*</td>'
            values = re.findall(pattern, tr_clear)
            trs.remove(tr)
            break
        else:
            values = []
    return text_of_first_td, values

if __name__ == '__main__':
    sample = """
<html>
    <head><title>test sample</title></head>
    <body>
        <table>
            <tr><td>KEY_1</td><td>101</td><td>100.2</td></tr>
            <tr><td>KEY_2</td><td>201</td> <td>202.2</td></tr>
            <tr> <td>KEY_3</td><td></td><td></td> </tr>
        </table>
    </body>
</html>
"""
    table = crop_data_table(sample, 'KEY_1', 'KEY_3')
    print 'table:', table

    trs = re.findall(r'<tr[\d\D]*?>\s*([\d\D]*?)\s*</tr>', table)
    key, values, trs = crop_key_tds('KEY_2', trs)
    print 'values:', values

    print 'vok2_1:', parse_number(values[0])
    print 'vok2_2:', parse_number(values[1])

