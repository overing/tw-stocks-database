#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pymongo
from datetime import datetime, date, timedelta
from bson.code import Code

def query_target(client, year, month, months_ago):
  '''
      查詢指定 year 年 month 月是 months_ago 個月以來創新高項目
  '''
  collection = client['db_stock']['colle_history']
  
  begin = end = date(year, month, 1)
  for i in xrange(0, months_ago):
    begin = begin.replace(day=1)
    begin = begin - timedelta(days=1)
    begin = begin.replace(day=1)
  begin = begin.year * 100 + begin.month
  end = end.year * 100 + end.month
  map = Code('''function() {
    var year = this['年'];
    var month = this['月'];
    var d = year * 100 + month;
    if (d >= %d && d <= %d)
      emit(this['code'], {
        'value': this['本月'],
        'year': year,
        'month': month,
        'count': 1
      });
  }''' % (begin, end))
  reduce = Code('''function(key, values) {
    var max = values[0];
    var count = values[0].count;
    for (var i = 1; i < values.length; i++) {
      var value = values[i];
      count += value['count'];
      if (value['value'] > max['value']) {
        max = value;
      }
    }
    max['count'] = count;
    return max;
  }''')
  collection = collection.map_reduce(map, reduce, 'results')
  spec = {
    'value.year': year, 
    'value.month': month, 
    'value.count': months_ago + 1
  }
  cursor = collection.find(spec=spec, sort=[ ( '_id', 1 ) ])
  return [int(item['_id']) for item in cursor] 
    
def query_history(client, code):
  collection = client['db_stock']['colle_history']
  spec = {'own_code': code}
  fields = [ 'own_code', u'年', u'月', u'本月' ]
  sort = [ ( 'own_code', 1 ), ( u'年', 1 ), ( u'月', 1 ) ]
  cursor = collection.find(spec=spec, fields=fields, sort=sort)
  for row in cursor:
    print('%s %d/%d: %d' % (row['own_code'], row[u'年'], row[u'月'], row[u'本月']))

def load_stock_place():
  data = {}
  import StockPlace
  with pymongo.MongoClient('localhost', 27017) as client:
    collection = client[StockPlace.DATABASE_NAME][StockPlace.COLLECTION_NAME]
    sort = [('code',1),('year',1),('month',1),('day',1)]
    with collection.find(sort=sort) as cursor:
      for document in cursor:
        code = document['code']
        if not code in data.keys():
          data[code] = {}
        
        year = document['year']
        if not year in data[code].keys():
          data[code][year] = {}
        
        month = document['month']
        if not month in data[code][year].keys():
          data[code][year][month] = {}
        
        day = document['day']
        data[code][year][month][day] = {
          'open': document['open'],
          'high': document['high'],
          'low': document['low'],
          'close': document['close'],
          'low': document['low']
        }
  
  return data

if __name__ == '__main__':
  start_time = datetime.now()
  print('Start %s' % start_time)
  
  data = load_stock_place()
  
  for year in data.keys():
    
  
  print(data.keys())
  
  end_time = datetime.now()
  print('Done %s, dt: %s' % (end_time, end_time - start_time))
