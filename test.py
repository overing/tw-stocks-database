#!/usr/bin/python
# -*- coding: utf-8 -*-

import datetime
import pymongo
import random
import re
import socket
import time
import urllib2

socket.setdefaulttimeout(30)

def FinancialRatiosComplite(client):
    collection = client['database_stock']['FinancialRatios']
    count = collection.count()

    collection = client['db_stock']['colle_stock']
    spec = { '$and': [
        { 'own_code': { '$regex': '^\d{4}$' } },
        { 'market': { '$ne': '興櫃' } },
        { '$or': [
            { 'issuer_type': '股票' }, { 'issuer_type': '普通股' }
        ]}
    ]}
    fields = [ 'own_code' ]
    sort = [ ( 'own_code', pymongo.ASCENDING ) ]
    cursor = collection.find(spec, fields, sort=sort)
    total = cursor.count() * 4 * 6
    cursor.close()
    
    print '%5.2f%% (%d/%d)' % ((100.0 * count / total), count, total)
    

if __name__ == '__main__':
    start_time = datetime.datetime.now()
    print 'start:', start_time
    
    with pymongo.MongoClient('localhost', 27017) as client:
        collection = client['database_stock']['FinancialRatios']
        count = collection.count()
        
        document = client['database_stock']['Stock'].find_one()
        for key in document.keys():
            try:
                print key, '=',
            except Exception as ex:
                print str(ex),
            try:
                print str(document[key])
            except Exception as ex:
                print str(ex)
        
        spec = { '類型': '股票', '市場': { '$in': ['上市', '上櫃'] }}
        fields = [ '代碼' ]
        sort = [ ( '代碼', pymongo.ASCENDING ) ]
        cursor = client['database_stock']['Stock'].find(spec, fields, sort=sort)
        total = cursor.count() * 24
        
        print('%5.2f%% (%d/%d)' % (count / total * 100, count, total))
    
    done_time = datetime.datetime.now()
    print 'done:', done_time, 'dt:', str(done_time - start_time)
    